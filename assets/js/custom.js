
  

$(document).ready(function(){

    // marker = new google.maps.Marker({
    //     icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
      
    // });

// menu

$('#mobile-nav').on("click",function(){
    $("#mySidenav2").addClass("width-menu");
    $("#cd-shadow-layer").css("display", "flex");
    $("body").css("position", "relative");
    $("body").css("overflow", "hidden");
    $("body").css("height", "100vh");
    $(".closebtn2").css("position", "fixed");
});
  
$('#close-mobile-nav').on("click",function(){
    $("#mySidenav2").removeClass("width-menu");
    $("#cd-shadow-layer").css("display", "none");
    $("body").css("position", "relative");
    $("body").css("overflow", "");
    $("body").css("height", "");
    $(".closebtn2").css("position", "relative");
  
});

$('.close-mobile-nav').on("click",function(){
    $("#mySidenav2").removeClass("width-menu");
    $("#cd-shadow-layer").css("display", "none");
    $("body").css("position", "relative");
    $("body").css("overflow", "");
    $("body").css("height", "");
    $(".closebtn2").css("position", "relative");
  
});


// serach bar 

$('#openSearch').on("click",function(){
    $('#myOverlay').css('display', 'block');
    $("body").css("position", "relative");
    $("body").css("overflow", "hidden");
    $("body").css("height", "100vh");
});

$('#closeSearch').on("click",function(){
    $('#myOverlay').css('display','none')
    $("body").css("position", "relative");
    $("body").css("overflow", "");
    $("body").css("height", "");
});

// wow


new WOW(
    {  
        mobile:  false,
    }
).init();
    
   

// nav slide




/// smooth scroll

$('a.smoth-scroll').on("click", function (e) {
    var anchor = $(this);
    $('html, body').stop().animate({
        scrollTop: $(anchor.attr('href')).offset().top - 70
    }, 1500);
    e.preventDefault();
});

// header sticky

var headertopoption = $(window);
var headTop = $('.navbar-dark');

headertopoption.on('scroll', function () {
    if (headertopoption.scrollTop() > 100) {
        headTop.addClass('fixed-top slideInDown animated');
    } else {
        headTop.removeClass('fixed-top slideInDown animated');
    }
});

// menu click

// $('.nav-tabs li a.active').click(function(){
//     $(".nav-tabs li a.active").removeClass("active");
//     $(this).addClass("active");
// });

$(".nav-link").click(function(){
    $(".nav-link").removeClass("active");
    $(this).addClass("active");
});


$(".nav-link").click(function(){
    $(".navbar-collapse").removeClass("show");

    $("#mySidenav2").removeClass("width-menu");
    $("#cd-shadow-layer").css("display", "none");
    $("body").css("position", "relative");
    $("body").css("overflow", "");
    $("body").css("height", "");
    $(".closebtn2").css("position", "relative");
});


// home

$(".url").click(function(){
    $(".url").removeClass("active");
    $(this).addClass("active");
});



$(".btn-link").click(function(){
    $(".card-header").removeClass("active");
    $(this).parent().parent().addClass("active");
   
});




// timeline js

$(".StepProgress li a").click(function(){
    $(".StepProgress li").removeClass("active fadeInUp");
    $(this).parent().addClass("active fadeInUp");
});


// readmore


$("#toggle-read").click(function() {
    var elem = $("#toggle-read").text();
    if (elem == "Read More...") {
      $("#toggle-read").text("Read Less");
      $("#text_hide_show").show();
    } else {
      $("#toggle-read").text("Read More...");
      $("#text_hide_show").hide();
    }
  });



// owl carousel our section

// home page banner




var owl = $('.testimonial-carousel').owlCarousel({
    loop:true,
    margin:60,
    smartSpeed:2000,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    dots: false,
    responsiveClass:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><i class="fe fe-arrow-right"></i></span>','<span class="span-roundcircle right-roundcircle"><i class="fe fe-arrow-left"></i></span>'],
    responsive:{
        0:{
            items:1,
            nav:false,
            dots: false,
        },
        600:{
            items:1,
            nav:false,
            dots: true,
        },

        1000:{
            items:1,
            nav: true,
            dots: false,
        },
  
    }
});

owl.on('changed.owl.carousel',function(property){
    var current = property.item.index;
    // var src = $(property.target).find(".owl-item").eq(current).find("img").attr('src');
    var src = $(property.target).find(".owl-item").eq(current).find('div').attr('class');
    console.log('Image current is ' + src);
    if((src)=="item zero"){
        $(".testimonial-thumb.active").removeClass("active");
        $(".testimonial-thumb.zero").addClass("active");
        $(".testimonial-thumb").parent().removeClass("active");
        $(".testimonial-thumb.zero").parent().addClass("active");
        
    };

    if((src)=="item one"){
        $(".testimonial-thumb.active").removeClass("active");
        $(".testimonial-thumb.one").addClass("active");
        $(".testimonial-thumb").parent().removeClass("active");
        $(".testimonial-thumb.one").parent().addClass("active");
    };

    if((src)=="item two"){
        $(".testimonial-thumb.active").removeClass("active");
        $(".testimonial-thumb.two").addClass("active");
        $(".testimonial-thumb").parent().removeClass("active");
        $(".testimonial-thumb.two").parent().addClass("active");
    };

    if((src)=="item three"){
        $(".testimonial-thumb.active").removeClass("active");
        $(".testimonial-thumb.three").addClass("active");
        $(".testimonial-thumb").parent().removeClass("active");
        $(".testimonial-thumb.three").parent().addClass("active");
    };

    if((src)=="item four"){
        $(".testimonial-thumb.active").removeClass("active");
        $(".testimonial-thumb.four").addClass("active");
        $(".testimonial-thumb").parent().removeClass("active");
        $(".testimonial-thumb.four").parent().addClass("active");
    };

    if((src)=="item five"){
        $(".testimonial-thumb.active").removeClass("active");
        $(".testimonial-thumb.five").addClass("active");
        $(".testimonial-thumb").parent().removeClass("active");
        $(".testimonial-thumb.five").parent().addClass("active");
    };

});


var owlwheel = $('.full-carousel').owlCarousel({
    margin:60,
    smartSpeed:2000,
    autoplay:false,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    dots: false,
    responsiveClass:true,
    loop:true,
    nav:false,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icon/arrow-2.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icon/arrow-1.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{
        0:{
            items:1,
            nav:false,
            dots: true,
        },
        600:{
            items:1,
            nav:false,
            dots: false,
        },

        1000:{
            items:2,
            nav:false,
            dots: false,
            loop:true
        },
        1025:{
            items:2,
            nav:false,
            stagePadding: 0,
            dots: false,
            loop:true
        }
    }
});

owlwheel.on('mousewheel', '.owl-stage', function (e) {
    if (e.deltaY>0) {
        owlwheel.trigger('next.owl');
    } else {
        owlwheel.trigger('prev.owl');
    }
    e.preventDefault();
});
// our service


$('.owl-carousel-banner').owlCarousel({
    loop:true,
    margin:10,
    smartSpeed:2000,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true,
    dots: false,
    responsiveClass:true,
    navText: ['<span class="span-roundcircle left-roundcircle"><img src="assets/images/icon/arrow-2.png" class="left_arrow_icon" alt="arrow" /></span>','<span class="span-roundcircle right-roundcircle"><img src="assets/images/icon/arrow-1.png" class="right_arrow_icon" alt="arrow" /></span>'],
    responsive:{
        0:{
            items:1,
            nav:false,
            dots: true,
        },
        600:{
            items:1,
            nav:false,
            dots: true,
        },

        1000:{
            items:1,
            nav:false,
            dots: true,
        },
        1025:{
            items:1,
            nav:false,
            dots: true,
            loop:true
        }
    }
});



 });





//  animation

if ($( window ).width() >= 1025) {
   

var controller = new ScrollMagic.Controller();




    new ScrollMagic.Scene({
        triggerElement: "#trigger0",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
        // offset: 100, // move trigger to center of element
    
    })
    // .addIndicators (
    //     {name: "reveal0" }
    // )
    .setClassToggle(".reveal0", "visible") // add class to reveal
    .addTo(controller);
    
// ----------------------------------------------------

	// build scene
    new ScrollMagic.Scene({
        triggerElement: "#trigger1",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
        // offset: 100, // move trigger to center of element

    })
    // .addIndicators (
    //     {name: "reveal1" }
    // )
    .setClassToggle(".reveal1", "visible") // add class to reveal
    .addTo(controller);


// ----------------------------------------------------

    new ScrollMagic.Scene({
        triggerElement: "#trigger2",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
        // offset: 100, // move trigger to center of element

    })
    .setClassToggle(".reveal2", "visible") // add class to reveal
    // .addIndicators (
    //     {name: "reveal2" }
    // )
    .addTo(controller);



    // ----------------------------------------------------

    new ScrollMagic.Scene({
        triggerElement: "#trigger3",
        triggerHook: 0.7, // show, when scrolled 10% into view
        duration: "120%", // hide 10% before exiting view (80% + 10% from bottom)
        // offset: 100, // move trigger to center of element

    })
    
    .setClassToggle(".reveal3", "visible") // add class to reveal
    // .addIndicators (
    //     {name: "reveal3" }
    // )
    .addTo(controller);


// ----------------------------------------------------
    
  //2
  
  var revealElements = document.getElementsByClassName("brand-item");
				for (var i=0; i<revealElements.length; i++) { // create a scene for each element
					new ScrollMagic.Scene({
										triggerElement: revealElements[i], // y value not modified, so we can use element as trigger as well
										// offset: 50,												 // start a little later
										triggerHook: 0.9,
									})
									.setClassToggle(revealElements[i], "visible") // add class toggle
									// .addIndicators({name: "brand-item" + (i+1) }) // add indicators (requires plugin)
									.addTo(controller);
                }



      //member
  
  var revealElements = document.getElementsByClassName("animate-box");
  for (var i=0; i<revealElements.length; i++) { // create a scene for each element
      new ScrollMagic.Scene({
                          triggerElement: revealElements[i], // y value not modified, so we can use element as trigger as well
                          // offset: 50,												 // start a little later
                          triggerHook: 0.6,
                      })
                      .setClassToggle(revealElements[i], "visible") // add class toggle
                    //   .addIndicators({name: "animate-box" + (i+1) }) // add indicators (requires plugin)
                      .addTo(controller);
  }            
                
// ----------------------------------------------------

  new ScrollMagic.Scene({
        triggerElement: "#trigger4",
        triggerHook: 0.9, // show, when scrolled 10% into view
        duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
        // offset: 0, // move trigger to center of element

    })
    
    .setClassToggle(".reveal4", "visible") // add class to reveal
    // .addIndicators (
    //     {name: "reveal4" }
    // )
    .addTo(controller);

// ----------------------------------------------------

    new ScrollMagic.Scene({
        triggerElement: "#trigger5",
        triggerHook: 0.8, // show, when scrolled 10% into view
        duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
        // offset: 0, // move trigger to center of element

    })
    
    .setClassToggle(".reveal5", "visible") // add class to reveal
    // .addIndicators (
    //     {name: "reveal5" }
    // )
    .addTo(controller);




// ----------------------------------------------------


new ScrollMagic.Scene({
    triggerElement: "#trigger6",
    triggerHook: 0.7, // show, when scrolled 10% into view
    duration: "100%", // hide 10% before exiting view (80% + 10% from bottom)
    // offset: 0, // move trigger to center of element

})

.setClassToggle(".reveal6", "visible") // add class to reveal
// .addIndicators (
//     {name: "reveal6" }
// )
.addTo(controller);

// ----------------------------------------------------

}